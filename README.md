# Combined CSC 471 and 419 Gaussian Blur Lab #

### Goal: Implement Gaussian Blur with both OpenGL/GLSL and CUDA ###

**Authors**: Adam Levasseur(419) and Kolton Yager(471)