#version 330 core

in vec2 texCoord;
out vec4 color;

uniform sampler2D texBuf;
uniform vec2 Resolution;

void main(){
	vec3 texcol = texture(texBuf, texCoord).rgb;

	color = vec4(texcol,1.0);
}
