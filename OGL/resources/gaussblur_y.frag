#version 330 core

#define oneover16 (1.0f/16.0f)

in vec2 texCoord;
out vec4 color;

uniform sampler2D texBuf;
uniform vec2 Resolution;

void main(){
	vec3 texcol;
	vec2 pixsize = 1.0/Resolution;

	vec3 sample;
	vec3 result = vec3(0.0);

	sample = texture(texBuf, texCoord + vec2(pixsize.x*2,0.0)).rgb;
	result += sample*1.0;

	sample = texture(texBuf, texCoord + vec2(pixsize.x*1,0.0)).rgb;
	result += sample*4.0;

	sample = texture(texBuf, texCoord + vec2(0.0,0.0)).rgb;
	result += sample*6.0;

	sample = texture(texBuf, texCoord + vec2(-pixsize.x*1,0.0)).rgb;
	result += sample*4.0;

	sample = texture(texBuf, texCoord + vec2(-pixsize.x*2,0.0)).rgb;
	result += sample*1.0;

	result*=oneover16;

	clamp(result,0.0,1.0);

	color = vec4(result,1.0);
}
