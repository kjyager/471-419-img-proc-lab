#version 330 core

in vec2 texCoord;
out vec4 color;
uniform sampler2D texBuf;

/* just pass through the texture color but add a little blue to anything on left side */
void main(){
   color = vec4(texture( texBuf, texCoord ).rgb, 1);

	if (mod(gl_FragCoord.x, 20) < 10 && mod(gl_FragCoord.y, 20) < 10) {
		color += vec4(0, 0, 0.5, 0);
	}

}
