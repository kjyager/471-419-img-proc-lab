#version 330 core

in vec2 texCoord;
out vec4 color;

uniform sampler2D texBuf;
uniform vec2 Resolution;

/* just pass through the texture color but add a little blue to anything on left side */
void main(){
	vec3 texcol;
	vec2 pixsize = 1.0/Resolution;

	if (mod(gl_FragCoord.y, 30) < 15) {
		texcol = texture( texBuf, texCoord + vec2(pixsize.x*9.0, 0.0)).rgb;
	}else{
		texcol = texture(texBuf, texCoord).rgb;
	}

	color = vec4(texcol,1.0);

}
